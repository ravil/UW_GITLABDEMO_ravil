//
//  main.m
//  UW_GITLABDEMO_ravil
//
//  Created by Lagadapati, Ravi on 2/12/15.
//  Copyright (c) 2015 TMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
